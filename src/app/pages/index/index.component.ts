import { Component, OnInit, OnDestroy } from "@angular/core";
import noUiSlider from "nouislider";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserService } from './../../user.service';

@Component({
  selector: "app-index",
  templateUrl: "index.component.html"
})
export class IndexComponent implements OnInit, OnDestroy {
  users: any = [[]];
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;
  constructor(
    protected userService: UserService
  ) {}
  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: "smooth" });
  }

  showTab = 1;
  tabToggle(index){
    this.showTab =index;
  }
  chunks(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }
  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("index-page");


    this.userService.getUsers()
    .subscribe(
      (data) => { // Success
        this.users = data;
        this.users = this.chunks(data,3);

      },
      (error) => {
        console.error(error);
      }
    );


    


  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("index-page");
  }
}
